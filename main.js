//Mensaje a AMP para cambio de altura
window.parent.postMessage({
    sentinel: 'amp',
    type: 'embed-size',
    height: document.body.scrollHeight
}, '*');

let urlParams = new URLSearchParams(window.location.search);
let typeParam = urlParams.get('card');

let svg = d3.select("#chart")
    .append("svg")
    .attr('id', 'svgdiv')
    .attr("width", document.getElementsByTagName('body')[0].clientWidth)
    .attr("height", document.getElementsByTagName('body')[0].clientHeight);

//Cada círculo representará 5 anuncios temporales
if(typeParam == 'madrid_1'){
    updateChart(101, 101); //Finales de marzo de 2019 --> 101
    svg.append("text")
        .attr("x", (document.getElementsByTagName('body')[0].clientWidth / 2))             
        .attr("y", document.getElementsByTagName('body')[0].clientHeight / 4.5)
        .attr("text-anchor", "middle")
        .style('font-family','Roboto')
        .style('fill', 'white') 
        .style("font-size", "16px")  
        .text("Cada círculo representa")
        .append('tspan')
        .attr("x", (document.getElementsByTagName('body')[0].clientWidth / 2))             
        .attr("y", document.getElementsByTagName('body')[0].clientHeight / 3.75)
        .style('font-family','Roboto')
        .style('fill', 'white') 
        .style("font-size", "16px")  
        .text('1 anuncio temporal');
} else if (typeParam == 'madrid_2'){
    updateChart(659, 101); //Mediados de junio de 2020 --> 659
} else if (typeParam == 'barcelona_1'){
    updateChart(252,252); //Finales de marzo de 2019 --> 252
} else if (typeParam == 'barcelona_2'){
    updateChart(711,252); //Mediados de junio de 2020 --> 711
}

function updateChart(data, initialData){
    let previousData = initialData;

    let circulos = [];
    for(let i = 0; i < data; i++){
        circulos[i] = {'name': '1'};
    }

    let nodo = d3.select('#nodo');
    nodo.remove();
    
    // Initialize the circle: all located at the center of the svg area
    let node = svg.append("g")
    .attr('id', 'nodo')
    .selectAll("circle")
    .data(circulos)
    .enter()
    .append("circle")
    .attr("r", 6.5)
    .attr("cx", document.getElementsByTagName('body')[0].clientWidth / 2)
    .attr("cy", document.getElementsByTagName('body')[0].clientHeight / 2)
    .style("fill", (d,i) => {return i < previousData ? '#a7c9c7' : '#ff6666'})
    .style("fill-opacity", 0.8)
    .style("stroke-width", 0.8);

    var simulation = d3.forceSimulation()
    .force("center", d3.forceCenter().x(document.getElementsByTagName('body')[0].clientWidth / 2).y(document.getElementsByTagName('body')[0].clientHeight / 2)) // Attraction to the center of the svg area
    .force("charge", d3.forceManyBody().strength(0.41))
    .force("collide", d3.forceCollide().strength(.51).radius(8));

    simulation
    .nodes(circulos)
    .on("tick", function(d){
        node
            .attr("cx", function(d){ return d.x; })
            .attr("cy", function(d){ return d.y; })
    });
}

function isMobile() {
    return window.innerWidth < 475
}

function isSmallMobile() {
    return window.innerWidth < 375
}
